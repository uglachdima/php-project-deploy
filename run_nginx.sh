#/bin/bash

docker stop kube-nginx
docker rm kube-nginx

NGINX_EXTERNAL_PORT=8092
DOCKER_BUILDKIT=1
docker build -t uglachdima/kube-nginx:base -f docker/separate/nginx/Dockerfile .
docker run -dp $NGINX_EXTERNAL_PORT:80 --name kube-nginx uglachdima/kube-nginx:base
