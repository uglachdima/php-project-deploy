<?php

declare(strict_types=1);

namespace App\Component\Monitor;

use App\Component\RdKafka\Producer;
use Laminas\Diagnostics\Check\CheckInterface;
use Laminas\Diagnostics\Result\AbstractResult;
use Laminas\Diagnostics\Result\Failure;
use Laminas\Diagnostics\Result\Success;
use RdKafka\Exception;

class KafkaPublishChecker implements CheckInterface
{
    private const TEST_TOPIC = 'test';

    private Producer $producer;

    public function __construct(Producer $producer)
    {
        $this->producer = $producer;
    }

    public function check(): AbstractResult
    {
        $content = json_encode(['time' => time()]);
        try {
            $this->producer->produce(self::TEST_TOPIC, $content);
        } catch (Exception $exception) {
            return new Failure($exception->getMessage());
        }
        return new Success(sprintf('Published to topic "%s" content %s', self::TEST_TOPIC, $content));
    }

    public function getLabel(): string
    {
        return 'Check Publish to Kafka';
    }
}
