#DevOps lesson

## Requirements:
- [docker compose](https://docs.docker.com/compose/install/)

## Installation:
- To clone the project
   ```
   git@gitlab.com:uglachdima/php-project.git
   ```
- Go to the folder
   ```
   cd php-project
   ```
- Build docker services
   ```
   docker-compose build
   ```
- Run project
   ```
   docker-compose up
   ```
- Install vendors
   ```
   docker-compose exec php-fpm composer install
   ```

## Project
### URLs:
* [Main page](http://localhost:8090/)
* [Health check](http://localhost:8090/monitor/health/)
* [Readiness probe](http://localhost:8090/monitor/health/http_status_checks?group=default)
* [Liveness probe](http://localhost:8090/monitor/health/http_status_checks?group=liveness)

### Commands
- Health check from console
   ```
   docker-compose exec php-fpm bin/console monitor:health
   ```
