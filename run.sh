#/bin/bash

docker stop kube-fpm
docker rm kube-fpm
docker stop kube-nginx
docker rm kube-nginx

SYMFONY_INTERNAL_PORT=9000
SYMFONY_EXTERNAL_PORT=8091
NGINX_EXTERNAL_PORT=8092
DOCKER_BUILDKIT=1

docker build -t uglachdima/kube-fpm:base -f docker/separate/fpm/Dockerfile .
docker run -dp $SYMFONY_EXTERNAL_PORT:$SYMFONY_INTERNAL_PORT --name kube-fpm uglachdima/kube-fpm:base
SYMFONY_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' kube-fpm)
echo "SYMFONY_IP: $SYMFONY_IP"
docker build --build-arg APP_HOST=$SYMFONY_IP --build-arg APP_PORT=$SYMFONY_INTERNAL_PORT -t uglachdima/kube-nginx:base -f docker/separate/nginx/Dockerfile .
docker run -dp $NGINX_EXTERNAL_PORT:80 --name kube-nginx uglachdima/kube-nginx:base
